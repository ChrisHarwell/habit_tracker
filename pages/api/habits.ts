// // pages
// //
// import * as habits from "../../lib/habits";
// const data = [
//   {
//     name: "Coding",
//     frequency: "Daily",
//     start_date: "2024-01-25",
//     duration_minutes: 28,
//     notes: "Generated dummy data",
//     completed_days: [
//       "2024-02-14",
//       "2024-03-12",
//       "2024-03-27",
//       "2024-04-03",
//       "2024-04-30",
//       "2024-08-08",
//       "2024-08-16",
//       "2024-12-21",
//     ],
//   },
//   {
//     name: "Reading",
//     frequency: "Daily",
//     start_date: "2024-01-24",
//     duration_minutes: 93,
//     notes: "Generated dummy data",
//     completed_days: [
//       "2024-01-25",
//       "2024-03-23",
//       "2024-03-25",
//       "2024-05-07",
//       "2024-08-08",
//       "2024-09-30",
//       "2025-01-07",
//       "2025-01-14",
//     ],
//   },
//   {
//     name: "Walking",
//     frequency: "Weekly",
//     start_date: "2023-07-22",
//     duration_minutes: 45,
//     notes: "Generated dummy data",
//     completed_days: [
//       "2023-07-25",
//       "2023-09-05",
//       "2023-11-30",
//       "2023-12-27",
//       "2024-02-27",
//     ],
//   },
//   {
//     name: "Yoga",
//     frequency: "Weekly",
//     start_date: "2023-03-16",
//     duration_minutes: 81,
//     notes: "Generated dummy data",
//     completed_days: ["2023-03-21", "2024-02-20"],
//   },
//   {
//     name: "Exercise",
//     frequency: "Monthly",
//     start_date: "2023-12-15",
//     duration_minutes: 85,
//     notes: "Generated dummy data",
//     completed_days: [
//       "2023-12-26",
//       "2024-04-22",
//       "2024-04-25",
//       "2024-06-07",
//       "2024-07-07",
//       "2024-08-08",
//       "2024-09-11",
//       "2024-10-08",
//     ],
//   },
//   {
//     name: "Walking",
//     frequency: "Daily",
//     start_date: "2023-10-14",
//     duration_minutes: 47,
//     notes: "Generated dummy data",
//     completed_days: ["2023-11-03", "2024-04-05", "2024-08-11", "2024-09-19"],
//   },
//   {
//     name: "Reading",
//     frequency: "Weekly",
//     start_date: "2023-12-04",
//     duration_minutes: 7,
//     notes: "Generated dummy data",
//     completed_days: [
//       "2023-12-31",
//       "2024-01-04",
//       "2024-02-26",
//       "2024-03-16",
//       "2024-03-29",
//       "2024-05-30",
//       "2024-06-12",
//       "2024-10-23",
//     ],
//   },
//   {
//     name: "Reading",
//     frequency: "Daily",
//     start_date: "2023-04-03",
//     duration_minutes: 101,
//     notes: "Generated dummy data",
//     completed_days: [
//       "2023-04-04",
//       "2023-05-27",
//       "2023-06-19",
//       "2023-08-01",
//       "2023-09-07",
//       "2023-10-11",
//       "2023-11-08",
//       "2023-11-26",
//       "2024-01-16",
//       "2024-02-27",
//     ],
//   },
//   {
//     name: "Coding",
//     frequency: "Monthly",
//     start_date: "2024-01-01",
//     duration_minutes: 50,
//     notes: "Generated dummy data",
//     completed_days: [
//       "2024-03-20",
//       "2024-04-06",
//       "2024-06-08",
//       "2024-07-18",
//       "2024-09-28",
//       "2024-10-15",
//       "2024-11-14",
//       "2024-11-17",
//     ],
//   },
//   {
//     name: "Exercise",
//     frequency: "Weekly",
//     start_date: "2023-10-19",
//     duration_minutes: 44,
//     notes: "Generated dummy data",
//     completed_days: [
//       "2023-12-04",
//       "2023-12-27",
//       "2024-02-09",
//       "2024-04-13",
//       "2024-04-18",
//       "2024-05-03",
//       "2024-07-19",
//       "2024-10-11",
//     ],
//   },
//   {
//     name: "Meditation",
//     frequency: "Weekly",
//     start_date: "2023-09-27",
//     duration_minutes: 104,
//     notes: "Generated dummy data",
//     completed_days: ["2023-12-28", "2024-01-07", "2024-04-12", "2024-08-19"],
//   },
//   {
//     name: "Reading",
//     frequency: "Monthly",
//     start_date: "2023-11-19",
//     duration_minutes: 91,
//     notes: "Generated dummy data",
//     completed_days: [
//       "2024-03-18",
//       "2024-03-21",
//       "2024-04-10",
//       "2024-09-13",
//       "2024-10-04",
//       "2024-10-21",
//     ],
//   },
//   {
//     name: "Learning a new language",
//     frequency: "Weekly",
//     start_date: "2024-02-05",
//     duration_minutes: 119,
//     notes: "Generated dummy data",
//     completed_days: [
//       "2024-03-04",
//       "2024-07-15",
//       "2024-07-19",
//       "2024-09-14",
//       "2025-01-03",
//     ],
//   },
//   {
//     name: "Learning a new language",
//     frequency: "Weekly",
//     start_date: "2023-11-15",
//     duration_minutes: 34,
//     notes: "Generated dummy data",
//     completed_days: [
//       "2024-01-09",
//       "2024-02-10",
//       "2024-04-24",
//       "2024-10-10",
//       "2024-10-27",
//     ],
//   },
//   {
//     name: "Walking",
//     frequency: "Monthly",
//     start_date: "2023-12-15",
//     duration_minutes: 114,
//     notes: "Generated dummy data",
//     completed_days: [
//       "2024-01-20",
//       "2024-04-21",
//       "2024-04-22",
//       "2024-10-17",
//       "2024-11-21",
//       "2024-12-08",
//     ],
//   },
//   {
//     name: "Coding",
//     frequency: "Daily",
//     start_date: "2023-11-14",
//     duration_minutes: 92,
//     notes: "Generated dummy data",
//     completed_days: ["2024-02-25", "2024-11-05"],
//   },
//   {
//     name: "Meditation",
//     frequency: "Monthly",
//     start_date: "2023-10-17",
//     duration_minutes: 80,
//     notes: "Generated dummy data",
//     completed_days: [
//       "2023-12-01",
//       "2024-02-08",
//       "2024-04-18",
//       "2024-06-02",
//       "2024-06-28",
//       "2024-07-06",
//     ],
//   },
//   {
//     name: "Walking",
//     frequency: "Weekly",
//     start_date: "2023-08-06",
//     duration_minutes: 25,
//     notes: "Generated dummy data",
//     completed_days: [
//       "2023-09-08",
//       "2023-12-02",
//       "2024-02-07",
//       "2024-03-18",
//       "2024-04-02",
//       "2024-07-12",
//     ],
//   },
//   {
//     name: "Walking",
//     frequency: "Daily",
//     start_date: "2023-05-26",
//     duration_minutes: 21,
//     notes: "Generated dummy data",
//     completed_days: [
//       "2023-08-18",
//       "2023-09-17",
//       "2023-11-24",
//       "2024-01-29",
//       "2024-02-10",
//       "2024-02-27",
//     ],
//   },
//   {
//     name: "Coding",
//     frequency: "Weekly",
//     start_date: "2023-07-03",
//     duration_minutes: 9,
//     notes: "Generated dummy data",
//     completed_days: [
//       "2023-09-13",
//       "2023-11-25",
//       "2024-01-27",
//       "2024-01-31",
//       "2024-02-02",
//       "2024-02-13",
//       "2024-04-01",
//       "2024-04-13",
//       "2024-04-15",
//       "2024-06-18",
//     ],
//   },
//   {
//     name: "Exercise",
//     frequency: "Weekly",
//     start_date: "2023-12-01",
//     duration_minutes: 32,
//     notes: "Generated dummy data",
//     completed_days: [
//       "2024-05-25",
//       "2024-05-29",
//       "2024-07-18",
//       "2024-11-05",
//       "2024-11-29",
//     ],
//   },
//   {
//     name: "Coding",
//     frequency: "Daily",
//     start_date: "2023-10-26",
//     duration_minutes: 102,
//     notes: "Generated dummy data",
//     completed_days: ["2024-03-30", "2024-06-23", "2024-09-01", "2024-09-13"],
//   },
//   {
//     name: "Meditation",
//     frequency: "Daily",
//     start_date: "2023-06-17",
//     duration_minutes: 16,
//     notes: "Generated dummy data",
//     completed_days: [
//       "2023-07-02",
//       "2023-07-16",
//       "2023-07-17",
//       "2023-07-26",
//       "2023-12-13",
//       "2023-12-22",
//       "2024-01-09",
//       "2024-02-11",
//       "2024-05-10",
//     ],
//   },
//   {
//     name: "Reading",
//     frequency: "Monthly",
//     start_date: "2023-08-18",
//     duration_minutes: 107,
//     notes: "Generated dummy data",
//     completed_days: [
//       "2023-09-02",
//       "2023-11-03",
//       "2024-03-12",
//       "2024-04-17",
//       "2024-07-11",
//     ],
//   },
//   {
//     name: "Exercise",
//     frequency: "Weekly",
//     start_date: "2024-02-23",
//     duration_minutes: 69,
//     notes: "Generated dummy data",
//     completed_days: [
//       "2024-03-05",
//       "2024-03-06",
//       "2024-04-04",
//       "2024-05-28",
//       "2024-11-25",
//       "2024-12-01",
//       "2024-12-03",
//       "2025-02-03",
//     ],
//   },
// ];

// export default async function handler(
//   req: { method: string; body: any },
//   res: {
//     status: (arg0: number) => {
//       (): any;
//       new(): any;
//       json: { (arg0: { message: string; habit?: any }): void; new(): any };
//       end: { (arg0: string): void; new(): any };
//     };
//     setHeader: (arg0: string, arg1: string[]) => void;
//   },
// ) {
//   if (req.method === "GET") {
//     res.status(200).json({ message: "Fetching habits" });
//     return res.status(200).json({ message: "", habit: await habits.list() });
//   } else if (req.method === "POST") {
//     // Extract the data sent in the request body
//     const habit = req.body;
//     // Normally, you would perform actions such as saving the data to a database
//     console.log(habit);
//     res.status(201).json({ message: "Habit added", habit });
//   } else {
//     res.setHeader("Allow", ["GET", "POST"]);
//     res.status(405).end(`Method ${req.method} Not Allowed`);
//   }
// }
//
//
// pages/api/habits.ts
import type { NextApiRequest, NextApiResponse } from "next";
import dbConnect from "../../lib/mongodb";
import Habit from "../../models/Habit";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  try {
    await dbConnect(); // Ensure the database connection is established
  } catch (error) {
    console.error("Failed to connect to database with error:", error);
  }

  switch (req.method) {
    case "GET":
      try {
        const habits = await Habit.find({});
        console.log(habits);
        res.status(200).json({ success: true, data: habits });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    case "POST":
      try {
        const habit = await Habit.create(req.body);
        res.status(201).json({ success: true, data: habit });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    // You can add more cases for PUT, DELETE, etc., following the same pattern
    default:
      res.setHeader("Allow", ["GET", "POST"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
  }
}
