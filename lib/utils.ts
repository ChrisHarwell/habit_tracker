import { type ClassValue, clsx } from "clsx";
import { twMerge } from "tailwind-merge";

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

type ISODateString = string; // Represents an ISO-8601 date string
export function isValidISODateString(
  dateString: string,
): dateString is ISODateString {
  // This regex pattern is designed to match the ISO-8601 date format closely.
  const regex =
    /^(\d{4}-[01]\d-[0-3]\d)(T[0-2]\d:[0-5]\d:[0-5]\d(\.\d+)?(([+-][0-2]\d:[0-5]\d)|Z)?)?$/;
  return regex.test(dateString);
}
