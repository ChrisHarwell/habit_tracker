// lib/habits.ts
import Habit from "../models/Habit"; // Adjust the path as necessary

export async function list() {
  return await Habit.find({}).sort({ _id: 1 });
}

export async function create(habitData: any) {
  const habit = new Habit(habitData);
  await habit.save();
  return habit;
}

export async function updateCompletedDates(
  habitId: string,
  completedDates: string[],
) {
  return await Habit.findByIdAndUpdate(
    habitId,
    { $set: { completedDates } },
    { new: true },
  );
}

export async function updateFrequency(habitId: string, frequency: string) {
  return await Habit.findByIdAndUpdate(
    habitId,
    { $set: { frequency } },
    { new: true },
  );
}

export async function updateCurrentStreak(
  habitId: string,
  currentStreak: number,
) {
  return await Habit.findByIdAndUpdate(
    habitId,
    { $set: { currentStreak } },
    { new: true },
  );
}

export async function updateLongestStreak(
  habitId: string,
  longestStreak: number,
) {
  return await Habit.findByIdAndUpdate(
    habitId,
    { $set: { longestStreak } },
    { new: true },
  );
}

export async function toggleActive(habitId: string, active: boolean) {
  return await Habit.findByIdAndUpdate(
    habitId,
    { $set: { active } },
    { new: true },
  );
}
