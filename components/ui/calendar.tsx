"use client";

import { useState } from "react";

export function Calendar({ habitName }: { habitName: string }) {
  // State for the current month and year
  const getMonth = new Date().getMonth();
  const getYear = new Date().getFullYear();

  const [currentMonth, setCurrentMonth] = useState(getMonth);
  const [currentYear, setCurrentYear] = useState(getYear);
  const daysInMonth = getDaysInMonth(currentMonth, currentYear);
  const daysArray = Array.from({ length: daysInMonth }, (_, i) => i + 1);

  // Store the completed days as an array of day numbers
  const [completedDays, setCompletedDays] = useState<number[]>([]);

  // Handle form submission
  const handleSubmitHabitCompleted = async (event: {
    preventDefault: () => void;
  }) => {
    event.preventDefault(); // Prevent default form submission behavior

    // Define the data to send
    const postData = {
      name: habitName,
      frequency: "daily",
      currentStreak: 0,
      longestStreak: 0, // Assuming longestStreak should also start at 0
      completedDates: [...completedDays],
      active: true,
    };

    // Send the POST request
    try {
      const response = await fetch("/api/habits", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(postData),
      });

      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }

      // Handle response data or confirmation
      const data = await response.json();
      console.log("Success:", data);
      // Optionally reset the form or provide user feedback
    } catch (error) {
      console.error("Error:", error);
    }
  };
  // Function to move to the next year
  const nextYear = () => {
    setCurrentYear(currentYear + 1);
  };

  // Function to move to the previous year
  const previousYear = () => {
    setCurrentYear(currentYear - 1);
  };
  // Function to move to the next month
  const nextMonth = () => {
    setCurrentMonth((prevMonth) => {
      if (prevMonth === 11) {
        nextYear();
        return 0;
      } else {
        return prevMonth + 1;
      }
    });
  };

  // Function to move to the previous month
  const previousMonth = () => {
    setCurrentMonth((prevMonth) => {
      if (prevMonth === 0) {
        console.log(prevMonth);
        previousYear();
        return 11;
      } else {
        return prevMonth - 1;
      }
    });
  };

  // Formatting the month for display
  const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  const displayMonth = monthNames[currentMonth];

  const toggleDayCompletion = (day: number) => {
    setCompletedDays((currentDays) => {
      // Check if the day is already marked as completed
      if (currentDays.includes(day)) {
        handleSubmitHabitCompleted;

        // If so, remove it from the array
        return currentDays.filter((d) => d !== day);
      } else {
        // Otherwise, add it to the array
        return [...currentDays, day];
      }
    });
  };
  return (
    <div className="rounded-none border-t border-gray-200 dark:border-gray-800">
      <div className="flex flex-col gap-4 ">
        <div className="grid gap-4 border border-black rounded-xl p-4">
          <div className="flex items-center justify-between gap-4">
            <button
              onClick={previousMonth}
              className="inline-flex items-center justify-center whitespace-nowrap text-sm font-medium ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 border border-input bg-background hover:bg-accent hover:text-accent-foreground h-9 rounded-md px-3"
            >
              Previous
            </button>
            <h3 className="text-lg font-semibold">
              {displayMonth} {currentYear}
            </h3>
            <button
              onClick={nextMonth}
              className="inline-flex items-center justify-center whitespace-nowrap text-sm font-medium ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 border border-input bg-background hover:bg-accent hover:text-accent-foreground h-9 rounded-md px-3"
            >
              Next
            </button>
          </div>

          <div className="grid grid-cols-7 gap-2">
            {daysArray.map((day) => (
              <button
                key={day}
                onClick={() => toggleDayCompletion(day)}
                className={`inline-flex items-center justify-center whitespace-nowrap rounded-md text-sm font-medium ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 border border-input w-full ${
                  completedDays.includes(day)
                    ? "bg-blue-500 text-white" // Use Tailwind's color for Dodger Blue, or specify your custom class/CSS
                    : "bg-background hover:bg-accent hover:text-accent-foreground"
                }`}
              >
                {day}
              </button>
            ))}
          </div>
          {/* Pass the currentMonth and currentYear as props to DaysButtons */}
        </div>
      </div>
    </div>
  );
}

function getDaysInMonth(month: number, year: number) {
  return new Date(year, month + 1, 0).getDate();
}
