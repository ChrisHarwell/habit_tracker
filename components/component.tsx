import { Button } from "@/components/ui/button";
import {
  DropdownMenuTrigger,
  DropdownMenuItem,
  DropdownMenuContent,
  DropdownMenu,
} from "@/components/ui/dropdown-menu";
import {
  CardTitle,
  CardHeader,
  CardContent,
  Card,
  CardDescription,
} from "@/components/ui/card";
import { Calendar } from "@/components/ui/calendar";
import { ResponsiveLine } from "@nivo/line";
// import useSWR from "swr";
import {
  ClassAttributes,
  HTMLAttributes,
  JSX,
  SetStateAction,
  useState,
} from "react";
export function Component() {
  // Store the completed days as an array of day numbers
  const [completedDays, setCompletedDays] = useState([]);
  const [selectedHabit, setSelectedHabit] = useState("");
  const [habitData, setHabitData] = useState({ streak: 0, longestStreak: 20 }); // Example structure, adjust based on actual API response
  // State to store the input value
  const [habitName, setHabitName] = useState("");
  const handleSelectHabit = async (habit: any) => {
    try {
      // Assuming the API is meant to return data based on a specific habit later on
      // For now, it returns all habits since the handler does not yet use the query
      const response = await fetch(`/api/habits`);
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      const data = await response.json();
      console.log(data); // Log the data to see if the request was successful
      setHabitData(data); // Adjust based on how you wish to use the data
      setSelectedHabit(habit);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const toggleDayCompletion = (day: any) => {
    setCompletedDays((currentDays: any) => {
      if (currentDays.includes(day)) {
        return currentDays.filter((d: any) => d !== day);
      } else {
        return [...currentDays, day];
      }
    });
  };

  // Update state on input change
  const handleChange = (event: {
    target: { value: SetStateAction<string> };
  }) => {
    setHabitName(event.target.value);
  };

  // Handle form submission
  const handleSubmit = async (event: { preventDefault: () => void }) => {
    event.preventDefault(); // Prevent default form submission behavior

    // Define the data to send
    const postData = {
      name: habitName,
      frequency: "daily",
      currentStreak: 0,
      longestStreak: 0, // Assuming longestStreak should also start at 0
      completedDates: [],
      active: true,
    };

    // Send the POST request
    try {
      const response = await fetch("/api/habits", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(postData),
      });

      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }

      // Handle response data or confirmation
      const data = await response.json();
      console.log("Success:", data);
      // Optionally reset the form or provide user feedback
    } catch (error) {
      console.error("Error:", error);
    }
  };
  return (
    <>
      <form method="post" onSubmit={handleSubmit}>
        <DropdownMenu>
          <DropdownMenuTrigger asChild>
            <Button className="mx-auto" variant="outline">
              Select Habit
            </Button>
          </DropdownMenuTrigger>
          <DropdownMenuContent align="center">
            <DropdownMenuItem onSelect={() => handleSelectHabit("habit")}>
              Exercise
            </DropdownMenuItem>
            <DropdownMenuItem onSelect={() => handleSelectHabit("Meditation")}>
              Meditation
            </DropdownMenuItem>
            <DropdownMenuItem onSelect={() => handleSelectHabit("Reading")}>
              Reading
            </DropdownMenuItem>
          </DropdownMenuContent>
        </DropdownMenu>
        <input type="text" value={habitName} onChange={handleChange}></input>
        <button type="submit">Submit form</button>
      </form>
      <div className="grid items-start gap-4 sm:grid-cols-2">
        <div className="flex flex-col gap-4">
          <Card>
            <CardHeader className="p-0">
              <CardTitle className="text-2xl font-bold">
                Habit Completion - {selectedHabit}
              </CardTitle>
            </CardHeader>
            <CardContent className="p-0">
              <Calendar
                // className="rounded-none border-t border-gray-200 dark:border-gray-800"
                // completedDays={completedDays}
                // onSelect={toggleDayCompletion}
                habitName={selectedHabit}
              />
            </CardContent>
          </Card>
          <Card>
            <CardContent className="flex items-center justify-center p-8">
              <div className="grid w-full max-w-sm gap-1 text-center">
                <h3 className="text-sm font-medium leading-none">Streak</h3>
                <h2 className="text-4xl font-extrabold leading-none">
                  {habitData.streak} days
                </h2>
                <p className="text-sm font-medium leading-none text-gray-500 dark:text-gray-400">
                  Keep it up! Your longest streak is {habitData.longestStreak}{" "}
                  days.
                </p>
              </div>
            </CardContent>
          </Card>
        </div>
        <Card>
          <CardHeader className="flex flex-col gap-1">
            <CardTitle>Habit Completion -</CardTitle>
            <CardDescription>
              Your progress on completing the habit over time.
            </CardDescription>
          </CardHeader>
          <CardContent>
            <div className="flex items-center justify-center w-full aspect-video">
              <LineChart className="w-full aspect-[2/1]" />
            </div>
          </CardContent>
        </Card>
      </div>
      <Card>
        <CardHeader>
          <CardTitle>Habit Completion Progress</CardTitle>
          <CardDescription>
            Monthly habit completion percentages for all habits.
          </CardDescription>
        </CardHeader>
        <CardContent>
          <ul className="divide-y divide-gray-200 dark:divide-gray-800">
            <li className="flex justify-between items-center py-2">
              <span>January 2023</span>
              <div className="w-1/2 h-2 bg-gray-300 rounded-full" />
              <span>75%</span>
            </li>
            <li className="flex justify-between items-center py-2">
              <span>February 2023</span>
              <span>82%</span>
            </li>
            <li className="flex justify-between items-center py-2">
              <span>March 2023</span>
              <span>68%</span>
            </li>
          </ul>
        </CardContent>
      </Card>
    </>
  );
}

function LineChart(
  props: JSX.IntrinsicAttributes &
    ClassAttributes<HTMLDivElement> &
    HTMLAttributes<HTMLDivElement>,
) {
  return (
    <div {...props}>
      <ResponsiveLine
        data={[
          {
            id: "Desktop",
            data: [
              { x: "Jan", y: 43 },
              { x: "Feb", y: 137 },
              { x: "Mar", y: 61 },
              { x: "Apr", y: 145 },
              { x: "May", y: 26 },
              { x: "Jun", y: 154 },
            ],
          },
          {
            id: "Mobile",
            data: [
              { x: "Jan", y: 60 },
              { x: "Feb", y: 48 },
              { x: "Mar", y: 177 },
              { x: "Apr", y: 78 },
              { x: "May", y: 96 },
              { x: "Jun", y: 204 },
            ],
          },
        ]}
        margin={{ top: 10, right: 10, bottom: 40, left: 40 }}
        xScale={{
          type: "point",
        }}
        yScale={{
          type: "linear",
        }}
        axisTop={null}
        axisRight={null}
        axisBottom={{
          tickSize: 0,
          tickPadding: 16,
        }}
        axisLeft={{
          tickSize: 0,
          tickValues: 5,
          tickPadding: 16,
        }}
        colors={["#2563eb", "#e11d48"]}
        pointSize={6}
        useMesh={true}
        gridYValues={6}
        theme={{
          tooltip: {
            chip: {
              borderRadius: "9999px",
            },
            container: {
              fontSize: "12px",
              textTransform: "capitalize",
              borderRadius: "6px",
            },
          },
          grid: {
            line: {
              stroke: "#f3f4f6",
            },
          },
        }}
        role="application"
      />
    </div>
  );
}
