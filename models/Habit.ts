// models/Habit.ts
import mongoose, { Document } from "mongoose";

enum HabitFrequency {
  DAILY = "daily",
}

interface IHabit extends Document {
  name: string;
  frequency: HabitFrequency;
  currentStreak: number;
  longestStreak: number;
  completedDates: string[];
  active: boolean;
}

const habitSchema = new mongoose.Schema({
  name: { type: String, required: true },
  frequency: {
    type: String,
    required: true,
    enum: Object.values(HabitFrequency),
  },
  currentStreak: { type: Number, required: true, default: 0 },
  longestStreak: { type: Number, required: true, default: 0 },
  completedDates: { type: [String], default: [] },
  active: { type: Boolean, required: true, default: true },
});

export default mongoose.models.Habit ||
  mongoose.model<IHabit>("Habit", habitSchema);
